---
layout: post
title: Here is a New Post
subtitle: Things are cool, until they aren’t. What do you do when it feels like there is nothing left to do? How does this work?
---

### Another Header

#### AND ANOTHER

What's up world? how ya feeling? 

**Gonna put something bold here**

> Some quoted text here and here and here

> Some more quoted text
> and more quoted text

And blow your mind with *italics.  Nice right?*

* some bullets are nice, let's see how they look
* and more here too

1. Eventually we'll style some buttons and things cause I may have a few
2. I also need to see how images look here and style those accordingly :/


I'd like to see how `code behaves`

so here is a a whole block: 

```ruby
if you know me
	//you love me
elseif
	//you hate me
else 
	//you really dont know me
end
```


What about [a link to Google?](https://www.google.com)

 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum feugiat vulputate dui, a molestie turpis rhoncus nec. Phasellus tempus pulvinar massa eu dapibus. Integer gravida lacus in arcu ultrices varius. Nullam ligula nisi, pretium tristique blandit non, gravida vitae lectus. Praesent lacinia quam sit amet scelerisque ultricies. Donec non consectetur dui. Pellentesque nec ligula eu diam dapibus facilisis. Suspendisse volutpat odio eget tempus viverra. Duis sit amet enim in leo commodo gravida id eu dui. Donec et ante condimentum, pretium nibh vitae, scelerisque augue. Sed tempus condimentum consequat. Curabitur suscipit et lacus nec mollis. Mauris ornare elementum sapien, nec egestas elit aliquet sed. Donec quam augue, bibendum eu vulputate nec, efficitur eget magna.
