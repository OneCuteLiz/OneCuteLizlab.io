---
layout: post
title:  
---

For the past 2 weeks I didn't look like anything was wrong.  I don't look sick or hurt; in fact I was just noticing how my skin is _flawless_.  I've managed to uphold the daily routines, inclusive of showing up to work daily.  

But I'm depressed.

I have had trouble concentrating.
I've been periodically disoriented while walking.
I don't eat as much; I've lost weight as a result.
I have experienced actual, physical pain after crying.
...and this fatigue is no damn joke.









I felt _guilty_ about taking time off to heal my mind.

I took FMLA 3 times because I was having a baby.  Of course.  Everyone knows the mother will need time to physically recover. The same social allowance is provided for those who underwent major surgery or need time after an injury to recovery. 

Today, I didn't feel "as bad" as I did the entire week prior, and since I was able to put a smile on my face at work or not cry at all that day I questioned if a _break_ was even necessary.  Sure, I wasn't thinking about suicide, but I was still iritable, angry, and THE ISSUE WASN'T SOLVED. 